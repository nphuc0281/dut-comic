# COMIC APP

A mobile app for reading comic online.

## Technology used

- Android Native (Java) for GUI
- Get data from API and caching data using Retrofit
- Backend using Django API

## Directory structure

```
project
|
└───README.md
│
└───app/src/java
│   |
│   └───com.example.dutcomic
│      |
│      └───adapters
│      |
│      └───fragments
│      |
│      └───itemdecoration
│      |
│      └───listeners
|      |
│      └───models
|      |
│      └───viewmodels
|      |
│      └───MainActivity
│   
└───app/src/res
│      |
│      └───drawable
│      |
│      └───layout
│      |
│      └───menu
│      |
│      └───navigation
│      
└───...
```

## App screenshots

![](screenshots/UI1.png)

![](screenshots/UI2.png)

