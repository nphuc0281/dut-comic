package com.example.dutcomic.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.dutcomic.R;
import com.example.dutcomic.listeners.RecyclerViewItemListener;
import com.example.dutcomic.models.Category;

import java.util.List;


public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    public List<Category> listCategory;
    private RecyclerViewItemListener recyclerViewItemListener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView items;
        private final CardView cardView;

        public ViewHolder(View view) {
            super(view);
            items = (TextView) view.findViewById(R.id.tv_items);
            cardView = (CardView) view.findViewById(R.id.cv_category);
        }

        public TextView getItemsView() {
            return items;
        }
        public CardView getCardView() {
            return  cardView;
        }
    }

    public CategoryAdapter(List<Category> listCategory, RecyclerViewItemListener recyclerViewItemListener) {
        this.listCategory = listCategory;
        this.recyclerViewItemListener = recyclerViewItemListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.category_card, viewGroup, false);

        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.getItemsView().setText(listCategory.get(position).getName());
        viewHolder.getCardView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerViewItemListener.onClick(viewHolder.getBindingAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return listCategory.size();
    }
}
