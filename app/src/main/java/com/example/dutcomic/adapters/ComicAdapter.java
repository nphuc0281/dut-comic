package com.example.dutcomic.adapters;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.example.dutcomic.R;
import com.example.dutcomic.databinding.ComicCardBinding;
import com.example.dutcomic.listeners.RecyclerViewItemListener;
import com.example.dutcomic.models.Comic;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ComicAdapter
        extends RecyclerSwipeAdapter<ComicAdapter.GridHolder>
        implements Filterable {
    private List<Comic> listComic;
    public List<Comic> listFilterComic;
    private RecyclerViewItemListener recyclerViewItemListener;

    public ComicAdapter(List<Comic> listComic, RecyclerViewItemListener recyclerViewItemListener) {
        this.listComic = listComic;
        this.listFilterComic = new ArrayList<>(listComic);
        this.recyclerViewItemListener = recyclerViewItemListener;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.comic_card;
    }

    public static class GridHolder extends RecyclerView.ViewHolder
    {
        private final ComicCardBinding binding;
        public GridHolder(@NonNull ComicCardBinding itemBinding) {
            super(itemBinding.getRoot());
            binding = itemBinding;
            binding.comicCard.addDrag(
                    SwipeLayout.DragEdge.Right, itemView.findViewById(R.id.comic_card_back));
        }
    }

    @NonNull
    @Override
    public GridHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ComicCardBinding itemBinding = ComicCardBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        return new GridHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull GridHolder holder, int position) {
        holder.binding.comicCardFront.tvName.setText(listFilterComic.get(position).getName());
        Picasso.get().load(
                listFilterComic.get(position).getCover_img()).into(holder.binding.comicCardFront.ivCover);



        holder.binding.comicCardFront.cvFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerViewItemListener.onClick(holder.getBindingAdapterPosition());
            }
        });

        holder.binding.comicCardBack.cvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerViewItemListener.onClick(holder.getBindingAdapterPosition());
            }
        });

        holder.binding.comicCardBack.tvComicName.setText(listFilterComic.get(position).getName());
        holder.binding.comicCardBack.tvDescription.setText(listFilterComic.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return listFilterComic.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<Comic> filterComics = new ArrayList<>();
                if(constraint.toString().isEmpty()){
                    filterComics.addAll(listComic);

                }else{
                    for(Comic record : listComic) {
                        if ((record.getName().toLowerCase().contains(
                                constraint.toString().toLowerCase()))) {
                            filterComics.add(record);
                        }
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filterComics;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listFilterComic.clear();
                listFilterComic.addAll((List<Comic>) results.values);
                notifyDataSetChanged();
            }
        };
    }
}
