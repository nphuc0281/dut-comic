package com.example.dutcomic.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.dutcomic.R;
import com.example.dutcomic.listeners.RecyclerViewItemListener;
import com.example.dutcomic.models.Chapter;
import com.example.dutcomic.models.Image;
import com.squareup.picasso.Picasso;

import java.util.List;


public class ChapterDetailAdapter extends RecyclerView.Adapter<ChapterDetailAdapter.ViewHolder> {

    public List<Image> listImages;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivImage;

        public ViewHolder(View view) {
            super(view);
            ivImage = itemView.findViewById(R.id.iv_image_comic);
        }

    }

    public ChapterDetailAdapter(List<Image> listImages) {
        this.listImages = listImages;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.image_item, viewGroup, false);

        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Picasso.get().load(
                listImages.get(position).getImage_url()).into(viewHolder.ivImage);
    }

    @Override
    public int getItemCount() {
        return listImages.size();
    }
}
