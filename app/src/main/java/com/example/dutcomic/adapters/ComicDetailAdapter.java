package com.example.dutcomic.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.dutcomic.R;
import com.example.dutcomic.listeners.RecyclerViewItemListener;
import com.example.dutcomic.models.Chapter;

import java.util.List;


public class ComicDetailAdapter extends RecyclerView.Adapter<ComicDetailAdapter.ViewHolder> {

    public List<Chapter> listChapter;
    private RecyclerViewItemListener recyclerViewItemListener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView items;
        private final CardView cardView;

        public ViewHolder(View view) {
            super(view);
            items = (TextView) view.findViewById(R.id.tv_chapter_items);
            cardView = (CardView) view.findViewById(R.id.chapter_item);
        }

        public TextView getItemsView() {
            return items;
        }
        public CardView getChapterItem() {
            return cardView;
        }
    }

    public ComicDetailAdapter(List<Chapter> listChapter, RecyclerViewItemListener recyclerViewItemListener) {
        this.listChapter = listChapter;
        this.recyclerViewItemListener = recyclerViewItemListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.chapter_item, viewGroup, false);

        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.getItemsView().setText(listChapter.get(position).getName());
        viewHolder.getChapterItem().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerViewItemListener.onClick(viewHolder.getBindingAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return listChapter.size();
    }
}
