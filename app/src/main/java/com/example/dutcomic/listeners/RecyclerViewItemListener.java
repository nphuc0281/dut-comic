package com.example.dutcomic.listeners;

public interface RecyclerViewItemListener {
    void onClick(int position);
}
