package com.example.dutcomic.fragments;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.dutcomic.R;
import com.example.dutcomic.adapters.ComicAdapter;
import com.example.dutcomic.databinding.FragmentComicListBinding;
import com.example.dutcomic.itemdecoration.GridSpacingItemDecoration;
import com.example.dutcomic.listeners.RecyclerViewItemListener;
import com.example.dutcomic.viewmodels.comic.ComicHomeViewModel;
import com.example.dutcomic.viewmodels.comic.ComicViewModel;


public class ComicListFragment extends Fragment {
    private FragmentComicListBinding binding;
    ComicHomeViewModel comicHomeViewModel;
    private ComicAdapter hotComicsAdapter;
    private ComicAdapter newChapterComicsAdapter;
    private ComicAdapter newComicsAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentComicListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.rvHotComics.setLayoutManager(new GridLayoutManager(getContext(),2));
        binding.rvHotComics.addItemDecoration(new GridSpacingItemDecoration(2, 10, true));
        binding.rvHotComics.setNestedScrollingEnabled(false);

        binding.rvNewChapterComics.setLayoutManager(new GridLayoutManager(getContext(),2));
        binding.rvNewChapterComics.addItemDecoration(new GridSpacingItemDecoration(2, 10, true));
        binding.rvNewChapterComics.setNestedScrollingEnabled(false);

        binding.rvNewComics.setLayoutManager(new GridLayoutManager(getContext(),2));
        binding.rvNewComics.addItemDecoration(new GridSpacingItemDecoration(2, 10, true));
        binding.rvNewComics.setNestedScrollingEnabled(false);

        comicHomeViewModel = new ViewModelProvider(this).get(ComicHomeViewModel.class);
        comicHomeViewModel.getComicHome(getContext()).observe(getViewLifecycleOwner(), comicHome -> {

            hotComicsAdapter = new ComicAdapter(comicHome.getHot_comics(), new RecyclerViewItemListener() {
                @Override
                public void onClick(int position) {
                    ComicDetailFragment comicDetailFragment = new ComicDetailFragment(comicHome.getHot_comics().get(position));
                    load_ComicDetailFragment(comicDetailFragment);
                }
            });
            binding.rvHotComics.setAdapter(hotComicsAdapter);

            newChapterComicsAdapter = new ComicAdapter(comicHome.getNew_chapter_comics(), new RecyclerViewItemListener() {
                @Override
                public void onClick(int position) {
                    ComicDetailFragment comicDetailFragment = new ComicDetailFragment(comicHome.getNew_chapter_comics().get(position));
                    load_ComicDetailFragment(comicDetailFragment);
                }
            });
            binding.rvNewChapterComics.setAdapter(newChapterComicsAdapter);

            newComicsAdapter = new ComicAdapter(comicHome.getNew_comics(), new RecyclerViewItemListener() {
                @Override
                public void onClick(int position) {
                    ComicDetailFragment comicDetailFragment = new ComicDetailFragment(comicHome.getNew_comics().get(position));
                    load_ComicDetailFragment(comicDetailFragment);
                }
            });
            binding.rvNewComics.setAdapter(newComicsAdapter);

            binding.tvHotComic.setVisibility(View.VISIBLE);
            binding.tvNewChapterComics.setVisibility(View.VISIBLE);
            binding.tvNewComic.setVisibility(View.VISIBLE);

            view.findViewById(R.id.loadingPanel).setVisibility(View.GONE);
        });
    }

    private void load_ComicDetailFragment(ComicDetailFragment comicDetailFragment){
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainerView, comicDetailFragment);
        transaction.addToBackStack("Comic");
        transaction.commit();
    }
}