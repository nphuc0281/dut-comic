package com.example.dutcomic.fragments;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.dutcomic.R;
import com.example.dutcomic.adapters.ComicAdapter;
import com.example.dutcomic.databinding.FragmentComicSearchBinding;
import com.example.dutcomic.itemdecoration.GridSpacingItemDecoration;
import com.example.dutcomic.listeners.RecyclerViewItemListener;
import com.example.dutcomic.viewmodels.comic.ComicViewModel;

public class ComicSearchFragment extends Fragment {

    FragmentComicSearchBinding binding;
    ComicViewModel comicViewModel;
    ComicAdapter comicAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentComicSearchBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.rvSearch.setLayoutManager(new GridLayoutManager(getContext(),2));
        binding.rvSearch.addItemDecoration(new GridSpacingItemDecoration(2, 10, true));

        comicViewModel = new ViewModelProvider(this).get(ComicViewModel.class);
        comicViewModel.getListComic(getContext()).observe(getViewLifecycleOwner(), list -> {
            comicAdapter = new ComicAdapter(list, new RecyclerViewItemListener() {
                @Override
                public void onClick(int position) {
                    onItemSearchClick(position);
                }
            });
            binding.rvSearch.setAdapter(comicAdapter);
            binding.tvSearch.setVisibility(View.VISIBLE);
            binding.loadingPanel.setVisibility(View.GONE);
        });
        setHasOptionsMenu(true);
    }

    public void onItemSearchClick(int position){
        ComicDetailFragment comicDetailFragment = new ComicDetailFragment(comicAdapter.listFilterComic.get(position));
        load_ComicDetailFragment(comicDetailFragment);
    }

    private void load_ComicDetailFragment(ComicDetailFragment comicDetailFragment){
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainerView, comicDetailFragment);
        transaction.addToBackStack("ComicSearch");
        transaction.commit();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem menuItem = menu.findItem(R.id.app_bar_search);
        SearchView searchView = (SearchView) menuItem.getActionView();

        ImageView searchIcon = searchView.findViewById(androidx.appcompat.R.id.search_button);
        searchIcon.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.ic_baseline_search_24));
        searchView.setQueryHint("Search comic...");

        SearchView.SearchAutoComplete searchAutoComplete = searchView.findViewById(androidx.appcompat.R.id.search_src_text);
        searchAutoComplete.setHintTextColor(Color.parseColor("#777777"));
        searchAutoComplete.setTextColor(Color.parseColor("#000000"));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setIconifiedByDefault(true);
        searchView.setIconified(false);
        searchView.clearFocus();

        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() == 0){
                    binding.tvSearch.setText("# All comics");
                } else {
                    binding.tvSearch.setText("# Search results");
                }
                comicAdapter.getFilter().filter(newText);
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }
}