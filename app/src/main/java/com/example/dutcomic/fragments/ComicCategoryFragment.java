package com.example.dutcomic.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dutcomic.R;
import com.example.dutcomic.adapters.CategoryAdapter;
import com.example.dutcomic.databinding.FragmentComicCategoryBinding;
import com.example.dutcomic.itemdecoration.GridSpacingItemDecoration;
import com.example.dutcomic.listeners.RecyclerViewItemListener;
import com.example.dutcomic.viewmodels.category.CategoryViewModel;

public class ComicCategoryFragment extends Fragment {
    private FragmentComicCategoryBinding binding;
    CategoryViewModel viewModel;
    private CategoryAdapter holderviewAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentComicCategoryBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.rvCategory.setLayoutManager(new GridLayoutManager(getContext(),2));
        binding.rvCategory.addItemDecoration(new GridSpacingItemDecoration(2, 10, true));

        viewModel = new ViewModelProvider(this).get(CategoryViewModel.class);
        viewModel.getListCategory(getContext()).observe(getViewLifecycleOwner(), list -> {
            holderviewAdapter = new CategoryAdapter(list, new RecyclerViewItemListener() {
                @Override
                public void onClick(int position) {
                    CategoryDetailFragment categoryDetailFragment = new CategoryDetailFragment(list.get(position));
                    load_CategoryDetailFragment(categoryDetailFragment);
                }
            });
            binding.rvCategory.setAdapter(holderviewAdapter);
            binding.tvCategory.setVisibility(View.VISIBLE);
            view.findViewById(R.id.loadingPanel).setVisibility(View.GONE);
        });
    }

    private void load_CategoryDetailFragment(CategoryDetailFragment categoryDetailFragment){
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainerView, categoryDetailFragment);
        transaction.addToBackStack("Category");
        transaction.commit();
    }
}