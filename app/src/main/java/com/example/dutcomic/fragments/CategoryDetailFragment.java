package com.example.dutcomic.fragments;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.dutcomic.R;
import com.example.dutcomic.adapters.ChapterDetailAdapter;
import com.example.dutcomic.adapters.ComicAdapter;
import com.example.dutcomic.databinding.FragmentComicListBinding;
import com.example.dutcomic.databinding.FragmentCategoryDetailBinding;
import com.example.dutcomic.itemdecoration.GridSpacingItemDecoration;
import com.example.dutcomic.listeners.RecyclerViewItemListener;
import com.example.dutcomic.models.Category;
import com.example.dutcomic.models.Chapter;
import com.example.dutcomic.models.Comic;
import com.example.dutcomic.viewmodels.chapter.ChapterViewModel;
import com.example.dutcomic.viewmodels.comic.ComicViewModel;
import com.example.dutcomic.viewmodels.comic.GetComicViewModel;

import java.util.List;

public class CategoryDetailFragment extends Fragment {

    FragmentCategoryDetailBinding binding;
    ComicAdapter comicAdapter;
    Category category;

    public CategoryDetailFragment(Category category) {
        this.category = category;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentCategoryDetailBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.rvCategory.setLayoutManager(new GridLayoutManager(getContext(),2));
        binding.rvCategory.addItemDecoration(new GridSpacingItemDecoration(2, 10, true));

        comicAdapter = new ComicAdapter(category.getComic(), new RecyclerViewItemListener() {
            @Override
            public void onClick(int position) {
                ComicDetailFragment comicDetailFragment = new ComicDetailFragment(category.getComic().get(position));
                load_ComicDetailFragment(comicDetailFragment);
            }
        });
        binding.rvCategory.setAdapter(comicAdapter);

        binding.tvCategoryName.setText("# Category: "+category.getName());
    }

    private void load_ComicDetailFragment(ComicDetailFragment comicDetailFragment){
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainerView, comicDetailFragment);
        transaction.addToBackStack("CategoryDetail");
        transaction.commit();
    }
}