package com.example.dutcomic.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dutcomic.R;
import com.example.dutcomic.adapters.ChapterDetailAdapter;
import com.example.dutcomic.databinding.FragmentChapterDetailBinding;
import com.example.dutcomic.itemdecoration.GridSpacingItemDecoration;
import com.example.dutcomic.models.Chapter;
import com.example.dutcomic.models.Comic;
import com.example.dutcomic.viewmodels.chapter.ChapterViewModel;

public class ChapterDetailFragment extends Fragment {
    private FragmentChapterDetailBinding binding;
    Chapter chapter;
    Comic comic;
    ChapterViewModel viewModel;
    ChapterDetailAdapter chapterDetailAdapter;

    public ChapterDetailFragment(Chapter chapter, Comic comic) {
        this.chapter = chapter;
        this.comic = comic;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentChapterDetailBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.setChapter(chapter);
        binding.setComic(comic);

        binding.rvImages.setLayoutManager(new GridLayoutManager(getContext(),1));
        binding.rvImages.addItemDecoration(new GridSpacingItemDecoration(1, 10, true));
        binding.rvImages.setNestedScrollingEnabled(false);

        viewModel = new ViewModelProvider(this).get(ChapterViewModel.class);
        viewModel.getChapter(getContext(), comic.getSlug(), chapter.getIndex()).observe(getViewLifecycleOwner(), chapter -> {
            chapterDetailAdapter = new ChapterDetailAdapter(chapter.getImages());
            binding.rvImages.setAdapter(chapterDetailAdapter);
            binding.tvComicName.setVisibility(View.VISIBLE);
            binding.tvChapterName.setVisibility(View.VISIBLE);
            view.findViewById(R.id.loadingPanel).setVisibility(View.GONE);
            if (chapterDetailAdapter.getItemCount() == 0){
                binding.tvTimeout.setVisibility(View.VISIBLE);
            }
        });
    }
}