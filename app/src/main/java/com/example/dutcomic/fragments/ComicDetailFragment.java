package com.example.dutcomic.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dutcomic.R;
import com.example.dutcomic.adapters.ComicAdapter;
import com.example.dutcomic.adapters.ComicDetailAdapter;
import com.example.dutcomic.databinding.FragmentComicDetailBinding;
import com.example.dutcomic.itemdecoration.GridSpacingItemDecoration;
import com.example.dutcomic.listeners.RecyclerViewItemListener;
import com.example.dutcomic.models.Chapter;
import com.example.dutcomic.models.Comic;
import com.example.dutcomic.viewmodels.comic.ComicHomeViewModel;
import com.example.dutcomic.viewmodels.comic.GetComicViewModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ComicDetailFragment extends Fragment {
    private Comic comic;
    private FragmentComicDetailBinding binding;
    ComicDetailAdapter comicDetailAdapter;
    GetComicViewModel getComicViewModel;
    List<Chapter> chapterList;

    public ComicDetailFragment(Comic comic) {
        this.comic = comic;
        this.chapterList = comic.getChapters();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        };
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentComicDetailBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.setComic(comic);
        Picasso.get().load(comic.getCover_img()).into(binding.ivComic);

        binding.rvChapters.setLayoutManager(new GridLayoutManager(getContext(),1));
        binding.rvChapters.addItemDecoration(new GridSpacingItemDecoration(1, 10, true));
        binding.rvChapters.setNestedScrollingEnabled(false);

        if (chapterList == null){
            getComicViewModel = new ViewModelProvider(this).get(GetComicViewModel.class);
            getComicViewModel.getComic(getContext(), comic.getSlug()).observe(getViewLifecycleOwner(), _comic -> {
                comicDetailAdapter = new ComicDetailAdapter(chapterList, new RecyclerViewItemListener() {
                    @Override
                    public void onClick(int position) {
                        comic = _comic;
                        for (Chapter c:
                                _comic.getChapters()) {
                            Log.d("DEBUG", c.getName());
                        }
                        ChapterDetailFragment chapterDetailFragment = new ChapterDetailFragment(
                                comic.getChapters().get(position),
                                comic);
                        load_ChapterDetailFragment(chapterDetailFragment);
                    }
                });
                binding.rvChapters.setAdapter(comicDetailAdapter);
            });
        } else {
            comicDetailAdapter = new ComicDetailAdapter(chapterList, new RecyclerViewItemListener() {
                @Override
                public void onClick(int position) {
                    ChapterDetailFragment chapterDetailFragment = new ChapterDetailFragment(
                            chapterList.get(position),
                            comic);
                    load_ChapterDetailFragment(chapterDetailFragment);
                }
            });
            binding.rvChapters.setAdapter(comicDetailAdapter);
        }
        view.findViewById(R.id.loadingPanel).setVisibility(View.GONE);
    }

    private void load_ChapterDetailFragment(ChapterDetailFragment chapterDetailFragment){
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainerView, chapterDetailFragment);
        transaction.addToBackStack("ComicDetail");
        transaction.commit();
    }
}