package com.example.dutcomic.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class Comic implements Serializable {
    @SerializedName("id")
    private UUID id;

    @SerializedName("name")
    private String name;

    @SerializedName("author")
    private String author;

    @SerializedName("description")
    private String description;

    @SerializedName("cover_img")
    private String cover_img;

    @SerializedName("slug")
    private String slug;

    @SerializedName("chapters")
    private List<Chapter> chapters;

    public Comic(String id, String name, String author, String description, String cover_img, String slug) {
        this.id = UUID.fromString(id);
        this.name = name;
        this.author = author;
        this.description = description;
        this.cover_img = cover_img;
        this.slug = slug;
    }

    public Comic(String id, String name, String author, String description, String cover_img, String slug, List<Chapter> chapters) {
        this.id = UUID.fromString(id);
        this.name = name;
        this.author = author;
        this.description = description;
        this.cover_img = cover_img;
        this.slug = slug;
        this.chapters = chapters;
    }

    public UUID getId() {
        return id;
    }

    public void setId(String id) {
        this.id = UUID.fromString(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCover_img() {
        return cover_img;
    }

    public void setCover_img(String cover_img) {
        this.cover_img = cover_img;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public List<Chapter> getChapters() {
        return chapters;
    }
}
