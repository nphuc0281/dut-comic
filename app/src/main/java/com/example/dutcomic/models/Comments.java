package com.example.dutcomic.models;

import java.util.UUID;

public class Comments {
    private UUID id;
    private String content;
    private Boolean liked;
    private String user;
    private String chapter;

    public Comments(UUID id, String content, Boolean liked, String user, String chapter) {
        this.id = id;
        this.content = content;
        this.liked = liked;
        this.user = user;
        this.chapter = chapter;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getLiked() {
        return liked;
    }

    public void setLiked(Boolean liked) {
        this.liked = liked;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }
}
