package com.example.dutcomic.models;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.UUID;

public class User {
    @SerializedName("id")
    private UUID id;

    @SerializedName("fullName")
    private String fullName;

    @SerializedName("phone")
    private String phone;

    @SerializedName("birthDay")
    private Date birthDay;

    @SerializedName("email")
    private String email;

    @SerializedName("avatar")
    private Image avatar;

    @SerializedName("account")
    private Account account;

    public User(UUID id, String fullName, String phone, Date birthDay, String email, Image avatar, Account account) {
        this.id = id;
        this.fullName = fullName;
        this.phone = phone;
        this.birthDay = birthDay;
        this.email = email;
        this.avatar = avatar;
        this.account = account;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Image getAvatar() {
        return avatar;
    }

    public void setAvatar(Image avatar) {
        this.avatar = avatar;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
