package com.example.dutcomic.models;

import java.util.UUID;

public class Image {
    private UUID id;
    private String image_url;
    private int index;
    private Chapter chaper;

    public Image(UUID id, String image_url, int index, Chapter chaper) {
        this.id = id;
        this.image_url = image_url;
        this.index = index;
        this.chaper = chaper;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Chapter getChaper() {
        return chaper;
    }

    public void setChaper(Chapter chaper) {
        this.chaper = chaper;
    }
}
