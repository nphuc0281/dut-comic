package com.example.dutcomic.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class ComicHome  implements Serializable {
    @SerializedName("new_comics")
    private List<Comic> new_comics;

    @SerializedName("new_chapter_comics")
    private List<Comic> new_chapter_comics;

    @SerializedName("hot_comics")
    private List<Comic> hot_comics;

    public ComicHome(List<Comic> new_comics, List<Comic> new_chapter_comics, List<Comic> hot_comics) {
        this.new_comics = new_comics;
        this.new_chapter_comics = new_chapter_comics;
        this.hot_comics = hot_comics;
    }

    public List<Comic> getNew_comics() {
        return new_comics;
    }

    public void setNew_comics(List<Comic> new_comics) {
        this.new_comics = new_comics;
    }

    public List<Comic> getNew_chapter_comics() {
        return new_chapter_comics;
    }

    public void setNew_chapter_comics(List<Comic> new_chapter_comics) {
        this.new_chapter_comics = new_chapter_comics;
    }

    public List<Comic> getHot_comics() {
        return hot_comics;
    }

    public void setHot_comics(List<Comic> hot_comics) {
        this.hot_comics = hot_comics;
    }
}
