package com.example.dutcomic.models;

import java.util.UUID;

public class Account {
    private UUID id;
    private String name;
    private Boolean is_staff;
    private Boolean role;

    public Account(UUID id, String name, Boolean is_staff, Boolean role) {
        this.id = id;
        this.name = name;
        this.is_staff = is_staff;
        this.role = role;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIs_staff() {
        return is_staff;
    }

    public void setIs_staff(Boolean is_staff) {
        this.is_staff = is_staff;
    }

    public Boolean getRole() {
        return role;
    }

    public void setRole(Boolean role) {
        this.role = role;
    }
}
