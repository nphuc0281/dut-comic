package com.example.dutcomic.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.UUID;

public class Chapter {
    @SerializedName("id")
    private UUID id;

    @SerializedName("comic")
    private String comic;

    @SerializedName("index")
    private int index;

    @SerializedName("name")
    private String name;

    @SerializedName("view")
    private int view;

    @SerializedName("Image")
    private List<Image> images;

    @SerializedName("Commentss")
    private Comments comments;

    public Chapter(UUID id, String comic, int index, String name, int view, List<Image> images,  Comments comments) {
        this.id = id;
        this.comic = comic;
        this.index = index;
        this.name = name;
        this.view = view;
        this.images = images;
        this.comments = comments;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getComic() {
        return comic;
    }

    public void setComic(String comic) {
        this.comic = comic;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public Comments getComments() {
        return comments;
    }

    public void setComments(Comments comments) {
        this.comments = comments;
    }
}
