package com.example.dutcomic.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.UUID;

public class Category {
    @SerializedName("id")
    private UUID id;

    @SerializedName("name")
    private String name;

    @SerializedName("slug")
    private String slug;

    @SerializedName("comic")
    private List<Comic> comic;

    public Category(UUID id, String name, String slug, List<Comic> comic) {
        this.id = id;
        this.name = name;
        this.slug = slug;
        this.comic = comic;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public List<Comic> getComic() {
        return comic;
    }

    public void setComic(List<Comic> comic) {
        this.comic = comic;
    }
}
