package com.example.dutcomic.viewmodels.category;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.dutcomic.models.Category;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class CategoryViewModel  extends ViewModel {
    private CategoryAPIService categoryAPIService;
    private MutableLiveData<List<Category>> listCategory;

    public MutableLiveData<List<Category>> getListCategory(Context context){
        if (listCategory == null){
            listCategory = new MutableLiveData<List<Category>>();
            categoryAPIService = new CategoryAPIService(context);
            categoryAPIService.getCategory()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<List<Category>>() {
                        @Override
                        public void onSuccess(@NonNull List<Category> categories) {
                            listCategory.setValue(categories);
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            listCategory.setValue(new ArrayList<>());
                            Log.d("DEBUG", e.getMessage());
                        }
                    });
        }
        return listCategory;
    }
}