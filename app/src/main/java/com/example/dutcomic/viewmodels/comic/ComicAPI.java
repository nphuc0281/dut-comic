package com.example.dutcomic.viewmodels.comic;

import com.example.dutcomic.models.Comic;

import java.util.List;

import io.reactivex.rxjava3.core.Single;
import retrofit2.http.GET;

public interface ComicAPI {
    @GET("/api/v1/comic/all")
    Single<List<Comic>> getComics();
}
