package com.example.dutcomic.viewmodels.comic;

import com.example.dutcomic.models.Comic;
import com.example.dutcomic.models.ComicHome;

import java.util.List;

import io.reactivex.rxjava3.core.Single;
import retrofit2.http.GET;

public interface ComicHomeAPI {
    @GET("/api/v1/comic/list_comic")
    Single<ComicHome> getComicHome();
}
