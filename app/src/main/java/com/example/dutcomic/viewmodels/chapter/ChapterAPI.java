package com.example.dutcomic.viewmodels.chapter;

import com.example.dutcomic.models.Chapter;
import com.example.dutcomic.models.Image;

import java.util.List;

import io.reactivex.rxjava3.core.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ChapterAPI {
    @GET("api/v1/comic/chapter/get_chapter")
    public Single<List<Chapter>> getChapter(@Query("slug") String slug, @Query("index") int index);
}
