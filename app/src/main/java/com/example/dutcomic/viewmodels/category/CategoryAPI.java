package com.example.dutcomic.viewmodels.category;

import com.example.dutcomic.models.Category;

import java.util.List;

import io.reactivex.rxjava3.core.Single;
import retrofit2.http.GET;

public interface CategoryAPI {
    @GET("/api/v1/comic/category/all")
    public Single<List<Category>> getCategory();
}
