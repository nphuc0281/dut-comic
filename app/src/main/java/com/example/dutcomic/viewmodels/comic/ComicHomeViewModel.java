package com.example.dutcomic.viewmodels.comic;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.dutcomic.models.Comic;
import com.example.dutcomic.models.ComicHome;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class ComicHomeViewModel extends ViewModel {
    private ComicHomeAPIService comicHomeAPIService;
    private MutableLiveData<ComicHome> comicHome;

    public MutableLiveData<ComicHome> getComicHome(Context context){
        if (comicHome == null){
            comicHome = new MutableLiveData<ComicHome>();
            comicHomeAPIService = new ComicHomeAPIService(context);
            comicHomeAPIService.getComicHome()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<ComicHome>() {
                        @Override
                        public void onSuccess(@NonNull ComicHome comics) {
                            comicHome.setValue(comics);
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {

                        }
                    });
        }
        return comicHome;
    }
}
