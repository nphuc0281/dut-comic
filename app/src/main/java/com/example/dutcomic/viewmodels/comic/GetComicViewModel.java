package com.example.dutcomic.viewmodels.comic;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.dutcomic.models.Comic;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class GetComicViewModel extends ViewModel {
    private GetComicAPIService getComicAPIService;
    private MutableLiveData<Comic> comic;

    public MutableLiveData<Comic> getComic(Context context, String slug){
        if (comic == null){
            comic = new MutableLiveData<Comic>();
            getComicAPIService = new GetComicAPIService(context);
            getComicAPIService.getComic(slug)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<Comic>() {
                        @Override
                        public void onSuccess(@NonNull Comic _comic) {
                            comic.setValue(_comic);
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            Log.d("DEBUG", e.getMessage());
                        }
                    });
        }
        return comic;
    }
}
