package com.example.dutcomic.viewmodels.comic;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.dutcomic.models.Comic;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class ComicViewModel extends ViewModel {
    private ComicAPIService comicAPIService;
    private MutableLiveData<List<Comic>> listComic;

    public MutableLiveData<List<Comic>> getListComic(Context context){
        if (listComic == null){
            listComic = new MutableLiveData<List<Comic>>();
            comicAPIService = new ComicAPIService(context);
            comicAPIService.getComics()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<List<Comic>>() {
                        @Override
                        public void onSuccess(@NonNull List<Comic> comics) {
                            listComic.setValue(comics);
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            listComic.setValue(new ArrayList<>());
                            Log.d("DEBUG", e.getMessage());
                        }
                    });
        }
        return listComic;
    }
}
