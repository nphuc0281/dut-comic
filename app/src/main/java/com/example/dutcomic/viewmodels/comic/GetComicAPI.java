package com.example.dutcomic.viewmodels.comic;

import com.example.dutcomic.models.Comic;

import java.util.List;

import io.reactivex.rxjava3.core.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetComicAPI {
    @GET("/api/v1/comic/get_comic")
    Single<Comic> getComics(@Query("slug") String slug);
}
