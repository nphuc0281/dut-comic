package com.example.dutcomic.viewmodels.chapter;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.dutcomic.models.Chapter;

import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class ChapterViewModel extends ViewModel {
    private ChapterAPIService chapterAPIService;
    private MutableLiveData<Chapter> chapter;

    public MutableLiveData<Chapter> getChapter(Context context, String slug, int index){
        if (chapter == null){
            chapter = new MutableLiveData<Chapter>();
            chapterAPIService = new ChapterAPIService(context);
            chapterAPIService.getChapter(slug, index)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<List<Chapter>>() {

                        @Override
                        public void onSuccess(@NonNull List<Chapter> chapters) {
                            chapter.setValue(chapters.get(0));
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            Log.d("DEBUG", e.getMessage());
                        }
                    });
        }
        return chapter;
    }
}