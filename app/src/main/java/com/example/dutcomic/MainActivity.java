package com.example.dutcomic;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;

import com.example.dutcomic.databinding.ActivityMainBinding;
import com.example.dutcomic.fragments.ComicCategoryFragment;
import com.example.dutcomic.fragments.ComicListFragment;
import com.example.dutcomic.fragments.ComicSearchFragment;
import com.google.android.material.navigation.NavigationBarView;

public class MainActivity extends AppCompatActivity{

    private ActivityMainBinding binding;
    ComicListFragment comicListFragment = new ComicListFragment();
    ComicCategoryFragment comicCategoryFragment = new ComicCategoryFragment();
    ComicSearchFragment comicSearchFragment = new ComicSearchFragment();
    Fragment currentFragment = comicListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String title = "DUT Comic";
        SpannableString s = new SpannableString(title);
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#0d6efd")), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
        getSupportActionBar().setTitle(s);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View viewRoot = binding.getRoot();
        setContentView(viewRoot);

        loadFragment(currentFragment);
        binding.bottomNavigationView.setOnItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private NavigationBarView.OnItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
                switch (item.getItemId()) {
                    case R.id.home:
                        currentFragment = comicListFragment;
                        break;

                    case R.id.category:
                        currentFragment = comicCategoryFragment;
                        break;

                    case R.id.search:
                        currentFragment = comicSearchFragment;
                        break;
                }

                return loadFragment(currentFragment);
            };

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentContainerView, fragment)
                    .commit();
            return true;
        }
        return false;
    }
}